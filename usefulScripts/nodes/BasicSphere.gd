@tool
@icon("./assets/SphereMesh.svg")
extends "res://usefulScripts/nodes/BasicShape.gd"
class_name BasicSphere3D

func new_mesh() -> PrimitiveMesh:
	return SphereMesh.new()
	
func new_shape() -> Shape3D:
	return SphereShape3D.new()
