@tool
@icon("./assets/CylinderMesh.svg")
extends "res://usefulScripts/nodes/BasicShape.gd"
class_name BasicCylinder3D

func new_mesh() -> PrimitiveMesh:
	return CylinderMesh.new()
	
func new_shape() -> Shape3D:
	return CylinderShape3D.new()
