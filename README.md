# starter project

You can use this starter project to quickly get started with your multidimensional walk.

# huge files

Some of the image files here are huuuuge. If you have limited disk space, you can download a version with smaller images.
For this, check out the `smallerfiles` branch like this:

![How to switch to version with smaller images](https://pointer.click/files/2024-03-25_11-00-26.mp4)

# minitutorial videos

These videos are too large to embed in here as a preview, but the link will get you there.

Here, for your convenience I documented how the starter project is created.
[How to create the starter project](https://pointer.click/files/2024-03-24_19-00-18.mp4)

And here a little one, how to adjust the sun to the sky.
[How to adjust sun to sky](https://pointer.click/files/2024-03-25_08-26-10.mp4)
